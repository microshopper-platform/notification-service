package com.microshopper.notificationservice.mapper;

import com.microshopper.notificationservice.model.Notification;
import com.microshopper.notificationservice.web.dto.NotificationDto;
import org.springframework.stereotype.Component;

@Component
public class NotificationMapper {

  public NotificationDto mapToNotificationDto(Notification notification) {

    NotificationDto notificationDto = new NotificationDto();
    notificationDto.setId(notification.getId());
    notificationDto.setTitle(notification.getTitle());
    notificationDto.setMessage(notification.getMessage());
    notificationDto.setUsername(notification.getUsername());
    notificationDto.setSeen(notification.getSeen());

    return notificationDto;
  }

  public Notification mapFromNotificationDto(NotificationDto notificationDto) {

    Notification notification = new Notification();
    notification.setTitle(notificationDto.getTitle());
    notification.setMessage(notificationDto.getMessage());
    notification.setUsername(notificationDto.getUsername());

    return notification;
  }
}
