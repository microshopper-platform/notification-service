package com.microshopper.notificationservice.mapper;

import com.microshopper.notificationservice.model.PushNotificationsSubscription;
import nl.martijndwars.webpush.Subscription;
import org.springframework.stereotype.Component;

@Component
public class PushNotificationsSubscriptionMapper {

  public PushNotificationsSubscription mapFromSubscription(Subscription subscription) {

    PushNotificationsSubscription pushNotificationsSubscription = new PushNotificationsSubscription();
    pushNotificationsSubscription.setEndpoint(subscription.endpoint);
    pushNotificationsSubscription.setAuth(subscription.keys.auth);
    pushNotificationsSubscription.setP256dh(subscription.keys.p256dh);

    return pushNotificationsSubscription;
  }

  public Subscription mapFromPushNotificationsSubscription(PushNotificationsSubscription pushNotificationsSubscription) {

    String endpoint = pushNotificationsSubscription.getEndpoint();
    Subscription.Keys keys =
      new Subscription.Keys(pushNotificationsSubscription.getP256dh(), pushNotificationsSubscription.getAuth());

    return new Subscription(endpoint, keys);
  }
}
