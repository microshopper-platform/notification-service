package com.microshopper.notificationservice.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import javax.annotation.PostConstruct;

@Configuration
public class MongoDatabaseConfiguration {

  private static final String NOTIFICATION_COLLECTION_NAME = "notification";

  private final ReactiveMongoTemplate reactiveMongoTemplate;

  @Autowired
  public MongoDatabaseConfiguration(ReactiveMongoTemplate reactiveMongoTemplate) {
    this.reactiveMongoTemplate = reactiveMongoTemplate;
  }

  @PostConstruct
  public void setup() {
    reactiveMongoTemplate.collectionExists(NOTIFICATION_COLLECTION_NAME).subscribe(exists -> {
      if (!exists) {
        CollectionOptions collectionOptions = CollectionOptions.empty().capped().size(8192).maxDocuments(100000);
        reactiveMongoTemplate.createCollection(NOTIFICATION_COLLECTION_NAME, collectionOptions).subscribe();
      }
    });
  }
}
