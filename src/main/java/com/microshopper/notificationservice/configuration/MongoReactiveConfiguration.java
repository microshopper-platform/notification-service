package com.microshopper.notificationservice.configuration;

import com.microshopper.notificationservice.repository.NotificationRepository;
import com.microshopper.notificationservice.repository.PushNotificationsSubscriptionRepository;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@EnableReactiveMongoRepositories(basePackageClasses = {
  NotificationRepository.class,
  PushNotificationsSubscriptionRepository.class})
public class MongoReactiveConfiguration extends AbstractReactiveMongoConfiguration {

  private static final String DATABASE_NAME = "notification_service_db";

  @Override
  @Bean
  public MongoClient reactiveMongoClient() {
    return MongoClients.create();
  }

  @Override
  protected String getDatabaseName() {
    return DATABASE_NAME;
  }

  @Bean
  public ReactiveMongoTemplate reactiveMongoTemplate() {
    return new ReactiveMongoTemplate(reactiveMongoClient(), getDatabaseName());
  }
}
