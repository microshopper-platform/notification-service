package com.microshopper.notificationservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonRootName("Notification")
public class NotificationDto {

  private String id;

  private String title;

  private String message;

  private String username;

  private Boolean seen;
}
