package com.microshopper.notificationservice.web.controller;

import com.microshopper.notificationservice.service.NotificationService;
import com.microshopper.notificationservice.web.dto.NotificationDto;
import io.swagger.v3.oas.annotations.tags.Tag;
import nl.martijndwars.webpush.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@Tag(name = "notification", description = "The Notification API")
public class NotificationController {

  private static final String BASE_URL = "/api/notifications";

  private final NotificationService notificationService;

  @Autowired
  public NotificationController(NotificationService notificationService) {
    this.notificationService = notificationService;
  }

  @GetMapping(value = BASE_URL + "/{username}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  @ResponseStatus(HttpStatus.OK)
  public Flux<NotificationDto> getAllNotificationsForUsername(@PathVariable String username) {
    return notificationService.getAllNotificationsForUsername(username);
  }

  @PostMapping(value = BASE_URL)
  @ResponseStatus(HttpStatus.CREATED)
  public void addNotification(@RequestBody NotificationDto notificationDto) {
    notificationService.addNotification(notificationDto);
  }

  @DeleteMapping(value = BASE_URL + "/{id}")
  @ResponseStatus(HttpStatus.OK)
  public void deleteNotification(@PathVariable String id) {
    notificationService.deleteNotification(id);
  }

  @PatchMapping(value = BASE_URL + "/seen/{id}")
  @ResponseStatus(HttpStatus.OK)
  public void markNotificationAsSeen(@PathVariable String id) {
    notificationService.markNotificationAsSeen(id);
  }

  @PostMapping(value = BASE_URL + "/push-notifications/subscribe/{username}")
  @ResponseStatus(HttpStatus.CREATED)
  public void subscribeForPushNotifications(@PathVariable String username, @RequestBody Subscription subscription) {
    notificationService.subscribeForPushNotifications(username, subscription);
  }

  @PostMapping(value = BASE_URL + "/push-notifications/send")
  @ResponseStatus(HttpStatus.CREATED)
  public void sendPushNotifications(@RequestBody NotificationDto notificationDto) {
    notificationService.sendPushNotification(notificationDto);
  }
}
