package com.microshopper.notificationservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.UUID;

@Getter
@Setter
@Document(collection = "push_notifications_subscription")
public class PushNotificationsSubscription {

  @MongoId
  @Field("_id")
  private String id = UUID.randomUUID().toString();

  @Field("username")
  private String username;

  @Field("endpoint")
  private String endpoint;

  @Field("p256dh")
  private String p256dh;

  @Field("auth")
  private String auth;
}
