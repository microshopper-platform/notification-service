package com.microshopper.notificationservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.UUID;

@Getter
@Setter
@Document(collection = "notification")
public class Notification {

  @MongoId
  @Field("_id")
  private String id = UUID.randomUUID().toString();

  @Field("title")
  private String title;

  @Field("message")
  private String message;

  @Field("username")
  private String username;

  @Field("seen")
  private Boolean seen = Boolean.FALSE;
}
