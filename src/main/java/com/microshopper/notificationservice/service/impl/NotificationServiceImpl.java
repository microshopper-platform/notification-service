package com.microshopper.notificationservice.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microshopper.notificationservice.mapper.NotificationMapper;
import com.microshopper.notificationservice.mapper.PushNotificationsSubscriptionMapper;
import com.microshopper.notificationservice.model.PushNotificationsSubscription;
import com.microshopper.notificationservice.repository.NotificationRepository;
import com.microshopper.notificationservice.repository.PushNotificationsSubscriptionRepository;
import com.microshopper.notificationservice.service.NotificationService;
import com.microshopper.notificationservice.web.dto.NotificationDto;
import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;
import nl.martijndwars.webpush.Subscription;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Security;
import java.util.concurrent.ExecutionException;

@Service
public class NotificationServiceImpl implements NotificationService {

  private static final String PUSH_NOTIFICATION_SUBJECT = "Microshopper Platform";

  @Value(value = "${push.service.public.key}")
  private String publicKey;

  @Value(value = "${push.service.private.key}")
  private String privateKey;

  private final NotificationRepository notificationRepository;
  private final PushNotificationsSubscriptionRepository pushNotificationsSubscriptionRepository;
  private final NotificationMapper notificationMapper;
  private final PushNotificationsSubscriptionMapper pushNotificationsSubscriptionMapper;
  private final ObjectMapper objectMapper;

  @Autowired
  public NotificationServiceImpl(
    NotificationRepository notificationRepository,
    PushNotificationsSubscriptionRepository pushNotificationsSubscriptionRepository,
    NotificationMapper notificationMapper,
    PushNotificationsSubscriptionMapper pushNotificationsSubscriptionMapper,
    ObjectMapper objectMapper) {

    this.notificationRepository = notificationRepository;
    this.pushNotificationsSubscriptionRepository = pushNotificationsSubscriptionRepository;
    this.notificationMapper = notificationMapper;
    this.pushNotificationsSubscriptionMapper = pushNotificationsSubscriptionMapper;
    this.objectMapper = objectMapper;
  }

  @Override
  public Flux<NotificationDto> getAllNotificationsForUsername(String username) {
    return notificationRepository
      .findByUsername(username)
      .map(notificationMapper::mapToNotificationDto);
  }

  @Override
  public void addNotification(NotificationDto notificationDto) {
    notificationRepository.save(notificationMapper.mapFromNotificationDto(notificationDto)).subscribe();
  }

  @Override
  public void deleteNotification(String id) {
    notificationRepository.deleteById(id).subscribe();
  }

  @Override
  public void markNotificationAsSeen(String id) {
    notificationRepository
      .findById(id)
      .flatMap(notification -> {
        notification.setSeen(true);
        return notificationRepository.save(notification);
      })
      .subscribe();
  }

  @Override
  public void subscribeForPushNotifications(String username, Subscription subscription) {
    pushNotificationsSubscriptionRepository
      .findPushNotificationsSubscriptionByUsername(username)
      .defaultIfEmpty(createDefaultPushNotificationsSubscription(username))
      .flatMap(pushNotificationsSubscription -> {
        pushNotificationsSubscription.setEndpoint(subscription.endpoint);
        pushNotificationsSubscription.setAuth(subscription.keys.auth);
        pushNotificationsSubscription.setP256dh(subscription.keys.p256dh);

        return pushNotificationsSubscriptionRepository.save(pushNotificationsSubscription);
      }).subscribe();
  }

  @Override
  public void sendPushNotification(NotificationDto notificationDto) {
    try {

      Security.addProvider(new BouncyCastleProvider());

      PushService pushService = new PushService(publicKey, privateKey, PUSH_NOTIFICATION_SUBJECT);

      pushNotificationsSubscriptionRepository
        .findPushNotificationsSubscriptionByUsername(notificationDto.getUsername())
        .subscribe(pushNotificationsSubscription -> notificationRepository
          .save(notificationMapper.mapFromNotificationDto(notificationDto))
          .subscribe(savedNotification -> {
            notificationDto.setSeen(savedNotification.getSeen());
            notificationDto.setId(savedNotification.getId());

            try {
              Subscription subscription =
                pushNotificationsSubscriptionMapper.mapFromPushNotificationsSubscription(pushNotificationsSubscription);

              String notificationJson = serializeNotificationDtoToJson(notificationDto);

              Notification pushNotification = new Notification(subscription, notificationJson);
              pushService.send(pushNotification);

            } catch (GeneralSecurityException | IOException | JoseException | ExecutionException | InterruptedException e) {
              e.printStackTrace();
            }
          }));

    } catch (GeneralSecurityException e) {
      e.printStackTrace();
    }
  }

  private PushNotificationsSubscription createDefaultPushNotificationsSubscription(String username) {
    PushNotificationsSubscription pushNotificationsSubscription = new PushNotificationsSubscription();
    pushNotificationsSubscription.setUsername(username);
    return pushNotificationsSubscription;
  }

  private String serializeNotificationDtoToJson(NotificationDto notificationDto) throws JsonProcessingException {
    return objectMapper.writeValueAsString(notificationDto);
  }
}
