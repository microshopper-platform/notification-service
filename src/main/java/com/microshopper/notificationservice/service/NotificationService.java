package com.microshopper.notificationservice.service;

import com.microshopper.notificationservice.web.dto.NotificationDto;
import nl.martijndwars.webpush.Subscription;
import reactor.core.publisher.Flux;

public interface NotificationService {

  Flux<NotificationDto> getAllNotificationsForUsername(String username);

  void addNotification(NotificationDto notificationDto);

  void deleteNotification(String id);

  void markNotificationAsSeen(String id);

  void subscribeForPushNotifications(String username, Subscription subscription);

  void sendPushNotification(NotificationDto notificationDto);
}
