package com.microshopper.notificationservice.repository;

import com.microshopper.notificationservice.model.PushNotificationsSubscription;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface PushNotificationsSubscriptionRepository
  extends ReactiveMongoRepository<PushNotificationsSubscription, String> {

  Mono<PushNotificationsSubscription> findPushNotificationsSubscriptionByUsername(String username);
}
