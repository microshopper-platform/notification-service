package com.microshopper.notificationservice.exception;

public class NotificationNotFoundException extends RuntimeException {

  public NotificationNotFoundException(String message) {
    super(message);
  }
}
